import type { ColorScheme } from '@mantine/core';
import { ColorSchemeProvider, MantineProvider } from '@mantine/core';
import { NotificationsProvider } from '@mantine/notifications';
import { useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';
import AppLayout from './layout/AppLayout';
import AddEmployee from './routes/AddEmployee';
import CreateUser from './routes/CreateUser';
import Dashboard from './routes/Dashboard';
import Departments from './routes/Departments';
import Employees from './routes/Employees';
import Login from './routes/Login';
import NotFound from './routes/NotFound';
import ViewEmployee from './routes/ViewEmployee';

function App() {
  const [colorScheme, setColorScheme] = useState<ColorScheme>('dark');

  const toggleColorScheme = (value?: ColorScheme) => setColorScheme(value || (colorScheme === 'dark' ? 'light' : 'dark'));

  return (
    <ColorSchemeProvider colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}>
      <MantineProvider theme={{ colorScheme }} withGlobalStyles withNormalizeCSS>
        <NotificationsProvider>
          <Routes>
            <Route
              path='/'
              element={
                <PrivateRoute>
                  <AppLayout />
                </PrivateRoute>
              }
            >
              <Route index element={<Dashboard />} />
              <Route path='employees' element={<Employees />} />
              <Route path='employee/:id' element={<ViewEmployee />} />
              <Route path='create_employee' element={<AddEmployee />} />
              <Route path='departments' element={<Departments />} />
              <Route path='create_user' element={<CreateUser />} />
            </Route>
            <Route path='login' element={<Login />} />
            <Route path='*' element={<NotFound />} />
          </Routes>
        </NotificationsProvider>
      </MantineProvider>
    </ColorSchemeProvider>
  )
}

export default App;
