import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

interface UserState {
   isSignedIn: boolean;
}

const initialState: UserState = {
   isSignedIn: false
}

export const userSlice = createSlice({
   name: 'user',
   initialState,
   reducers: {
      login: (state, action: PayloadAction<string>) => {
         sessionStorage.setItem('access_token', action.payload);
         state.isSignedIn = true;
      },
      logout: (state) => {
         sessionStorage.removeItem('access_token');
         state.isSignedIn = false;
      }
   }
});

export const { login, logout } = userSlice.actions;

export default userSlice.reducer;