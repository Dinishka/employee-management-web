import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const apiSlice = createApi({
   baseQuery: fetchBaseQuery({
      baseUrl: import.meta.env.VITE_BASE_URL,
      prepareHeaders: (headers) => {
         // get the access token from session storage
         const token = sessionStorage.getItem('access_token');
         if (token) {
            // set the authorization header with bearer token
            headers.set('Authorization', `Bearer ${token}`);
         }
         return headers;
      }
   }),
   // tag types for caching
   tagTypes: ['Employee', 'Department', 'Designation'],
   endpoints: () => ({})
});