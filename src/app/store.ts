import { configureStore } from '@reduxjs/toolkit';
import { apiSlice } from './apiSlice';
import userReducer from './userSlice';

const store = configureStore({
   reducer: {
      user: userReducer,
      [apiSlice.reducerPath]: apiSlice.reducer
   },
   middleware: getDefaultMiddleware => getDefaultMiddleware().concat(apiSlice.middleware)
});

export default store;

// types for state & dispatchers
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;