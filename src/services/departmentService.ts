import { apiSlice } from '../app/apiSlice';
import type { Department, Designation } from '../api-types';

export const departmentService = apiSlice.injectEndpoints({
   endpoints: builder => ({
      getDepartments: builder.query<Department[], void>({
         query: () => '/department',
         providesTags: ['Department']
      }),
      getDepartmentDesignations: builder.query<Designation[], string>({
         query: (departmentId) => `/department/${departmentId}/designations`,
         providesTags: ['Designation']
      }),
      createDepartment: builder.mutation({
         query: (newDepartment) => ({
            url: '/department',
            method: 'POST',
            body: newDepartment
         }),
         invalidatesTags: ['Department']
      })
   })
});

export const { useGetDepartmentsQuery, useLazyGetDepartmentDesignationsQuery, useCreateDepartmentMutation } = departmentService;