import { apiSlice } from '../app/apiSlice';
import type { Employee } from '../api-types';

export const employeeService = apiSlice.injectEndpoints({
   endpoints: builder => ({
      getEmployees: builder.query<Employee[], void>({
         query: () => '/employee',
         providesTags: ['Employee']
      }),
      getEmployeeById: builder.query<Employee, string>({
         query: (employeeId) => `employee/${employeeId}`,
         providesTags: (_result, _error, arg) => [{ type: 'Employee', id: arg }]
      }),
      createEmployee: builder.mutation({
         query: (newEmployee) => ({
            url: '/employee',
            method: 'POST',
            body: newEmployee,
         }),
         invalidatesTags: ['Employee']
      }),
      updateEmployee: builder.mutation({
         query: ({ employeeId, ...data }) => ({
            url: `/employee/${employeeId}`,
            method: 'PUT',
            body: data
         }),
         invalidatesTags: (_result, _error, arg) => ['Employee', { type: 'Employee', id: arg.employeeId }]
      }),
      deleteEmployee: builder.mutation({
         query: (employeeId) => ({
            url: `/employee/${employeeId}`,
            method: 'DELETE'
         }),
         invalidatesTags: ['Employee']
      })
   }),
});

export const {
   useGetEmployeesQuery,
   useGetEmployeeByIdQuery,
   useCreateEmployeeMutation,
   useUpdateEmployeeMutation,
   useDeleteEmployeeMutation
} = employeeService;