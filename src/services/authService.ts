import { apiSlice } from '../app/apiSlice';

export const authService = apiSlice.injectEndpoints({
   endpoints: builder => ({
      login: builder.mutation({
         query: (user) => ({
            url: '/auth/login',
            method: 'POST',
            body: user
         })
      }),
      register: builder.mutation({
         query: (newUser) => ({
            url: '/auth/register',
            method: 'POST',
            body: newUser
         })
      })
   })
});

export const { useLoginMutation, useRegisterMutation } = authService;