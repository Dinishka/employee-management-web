import { apiSlice } from '../app/apiSlice';

export const designationService = apiSlice.injectEndpoints({
   endpoints: builder => ({
      createDesignation: builder.mutation({
         query: (newDesignation) => ({
            url: '/designation',
            method: 'POST',
            body: newDesignation
         }),
         invalidatesTags: ['Designation']
      })
   })
});

export const { useCreateDesignationMutation } = designationService;