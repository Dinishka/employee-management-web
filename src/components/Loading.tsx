import { Center, createStyles, Loader } from '@mantine/core';

const useStyles = createStyles(() => ({
   center: {
      minHeight: 'calc(100vh - 100px)'
   }
}));

const Loading = () => {
   const {classes} = useStyles();
   
   return (
      <Center className={classes.center}>
         <Loader size='lg' />
      </Center>
   )
}

export default Loading;