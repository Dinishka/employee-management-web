import type { GroupProps } from '@mantine/core';
import { Button, Group } from '@mantine/core';

interface Props extends GroupProps {
   loading: boolean;
   reset: () => void;
}

const FormActions = ({ loading, reset, grow = true, ...other }: Props) => {
   return (
      <Group mt='xl' grow={grow} {...other}>
         <Button type='submit' loading={loading}>Submit</Button>
         <Button variant='default' onClick={reset}>Reset</Button>
      </Group>
   )
}

export default FormActions;