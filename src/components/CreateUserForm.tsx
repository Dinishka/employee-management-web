import { PasswordInput, SimpleGrid, TextInput } from '@mantine/core';
import { useForm, zodResolver } from '@mantine/form';
import { showNotification } from '@mantine/notifications';
import { Check, Lock, User, X } from 'tabler-icons-react';
import { z } from 'zod';
import { useRegisterMutation } from '../services/authService';
import FormActions from './FormActions';

const schema = z.object({
   username: z.string().trim().min(1, { message: 'Username is required!' }).max(45, { message: 'Username is too long!' }),
   password: z.string().min(1, { message: 'Password is required!' }),
   confirmPassword: z.string().min(1, { message: 'Password confirmation is required!' })
});

const CreateUserForm = () => {

   const { onSubmit, reset, getInputProps, values, setFieldError } = useForm({
      schema: zodResolver(schema),
      initialValues: {
         username: '',
         password: '',
         confirmPassword: ''
      }
   });

   const [register, { isLoading }] = useRegisterMutation()

   const handleSubmit = async (data: typeof values) => {
      // Check if password confirmation is success
      if (data.password === data.confirmPassword) {
         try {
            const payload = await register(data).unwrap();
            showNotification({
               title: 'Success',
               message: payload.message,
               color: 'teal',
               icon: <Check size={18} />
            });
            reset();
         } catch (e: any) {
            showNotification({
               title: 'Error',
               message: e.data.message,
               color: 'red',
               icon: <X size={18} />
            });
         }
      } else {
         setFieldError('confirmPassword', 'Password confirmation failed!');
      }
   }

   return (
      <form onSubmit={onSubmit(handleSubmit)}>
         <SimpleGrid cols={3}>
            <TextInput label='Username' placeholder='Enter username' required icon={<User size={16} />} {...getInputProps('username')} />
            <PasswordInput label='Password' placeholder='Enter password' required icon={<Lock size={16} />} {...getInputProps('password')} />
            <PasswordInput label='Confirm Password' placeholder='Confirm your password' required icon={<Lock size={16} />} {...getInputProps('confirmPassword')} />
         </SimpleGrid>
         <FormActions loading={isLoading} reset={reset} grow={false} />
      </form>
   );
}

export default CreateUserForm;