import { Box, Button, Text } from '@mantine/core';

interface Props {
   label: string;
   items: JSX.Element[];
   onAdd: React.MouseEventHandler<HTMLButtonElement>;
}

const FormList = ({ label, items, onAdd }: Props) => {
   return (
      <Box>
         <Text weight={500} size='sm' mb={4}>{label}</Text>
         {items}
         <Button variant='subtle' compact onClick={onAdd}>+</Button>
      </Box>
   )
}

export default FormList;