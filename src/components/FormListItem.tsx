import { ActionIcon, Group, TextInput } from '@mantine/core';
import type { GetInputPropsPayload } from '@mantine/form/lib/types';
import { Trash } from 'tabler-icons-react';

interface Props {
   placeholder: string;
   inputProps: GetInputPropsPayload;
   onRemove: React.MouseEventHandler<HTMLButtonElement>;
}

const FormListItem = ({ placeholder, inputProps, onRemove }: Props) => {
   return (
      <Group mb='xs'>
         <TextInput
            placeholder={placeholder}
            sx={{ flex: 1 }}
            {...inputProps}
         />
         <ActionIcon
            color='red'
            variant='hover'
            onClick={onRemove}
         >
            <Trash size={16} />
         </ActionIcon>
      </Group>
   )
}

export default FormListItem;