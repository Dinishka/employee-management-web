import { Divider, SimpleGrid, TextInput } from '@mantine/core';
import { DatePicker } from '@mantine/dates';
import { formList, useForm, zodResolver } from '@mantine/form';
import { randomId } from '@mantine/hooks';
import { showNotification } from '@mantine/notifications';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import { Calendar, Check, X } from 'tabler-icons-react';
import { z } from 'zod';
import type { Employee } from '../api-types';
import { useGetDepartmentsQuery, useLazyGetDepartmentDesignationsQuery } from '../services/departmentService';
import { useUpdateEmployeeMutation } from '../services/employeeService';
import CreateDepartmentModal from './CreateDepartmentModal';
import CreateDesignationModal from './CreateDesignationModal';
import FormActions from './FormActions';
import FormList from './FormList';
import FormListItem from './FormListItem';
import SelectWithAdd from './SelectWithAdd';

interface Props extends Employee {
   id: number;
}

const schema = z.object({
   first_name: z.string().trim().min(1, { message: 'First name is required!' }).max(45, { message: 'First name is too long!' }),
   last_name: z.string().trim().min(1, { message: 'Last name is required!' }).max(45, { message: 'Last name is too long!' }),
   birthday: z.date(),
   contact_numbers: z.array(z.object({
      contactNumber: z.string().trim().min(1, { message: 'Please specify a contact number!' }).max(10, {message: 'Invalid contsct number!'}),
      key: z.string().trim()
   })).nonempty('At least 1 contact number is required!'),
   addresses: z.array(z.object({
      address: z.string().trim().min(1, { message: 'Please specify a address' }),
      key: z.string().trim()
   })).nonempty('At least 1 address is required!'),
   nic: z.string().trim().min(10, { message: 'Invalid NIC!' }).max(12, { message: 'Invalid NIC!' }),
   department_id: z.string().trim().min(1, { message: 'Department is required!' }),
   designation_id: z.string().trim().min(1, { message: 'Designation is required!' })
});

const UpdateEmployeeForm = (props: Props) => {
   const { data: departments } = useGetDepartmentsQuery();
   const [fetchDesignations, { isLoading, isFetching, data: designations }] = useLazyGetDepartmentDesignationsQuery();

   useEffect(() => {
      fetchDesignations(props.department_id.toString());
   }, []);

   const [openNewDepartment, setOpenNewDepartment] = useState(false);
   const [openNewDesignation, setOpenNewDesignation] = useState(false);

   const { onSubmit, reset, getInputProps, values, errors, addListItem, getListInputProps, removeListItem, setFieldValue } = useForm({
      schema: zodResolver(schema),
      initialValues: {
         first_name: props.first_name,
         last_name: props.last_name,
         birthday: new Date(props.birthday),
         contact_numbers: formList(props.contact_numbers.map(contactNumber => ({ contactNumber, key: randomId() }))),
         addresses: formList(props.addresses.map(address => ({ address, key: randomId() }))),
         nic: props.nic,
         department_id: props.department_id.toString(),
         designation_id: props.designation_id.toString()
      }
   });

   const [updateEmployee, { isLoading: submitting }] = useUpdateEmployeeMutation();

   const handleSubmit = async (data: typeof values) => {
      const employee = {
         ...data,
         birthday: dayjs(data.birthday).format('YYYY-MM-DD'),
         contact_numbers: data.contact_numbers.map(({ contactNumber }) => contactNumber),
         addresses: data.addresses.map(({ address }) => address),
         employeeId: props.id
      }
      // @ts-ignore
      delete employee.department_id;
      try {
         const payload = await updateEmployee(employee).unwrap();
         showNotification({
            title: 'Success',
            message: payload.message,
            color: 'teal',
            icon: <Check size={18} />
         });
      } catch (e: any) {
         showNotification({
            title: 'Error',
            message: e.data.message,
            color: 'red',
            icon: <X size={18} />
         });
      }
   }

   const handleDepartmentChange = (newDepartment: string) => {
      setFieldValue('department_id', newDepartment);
      setFieldValue('designation_id', '');
      fetchDesignations(newDepartment);
   }

   const departmentItems = departments?.map(({ id, department_name }) => ({ value: id.toString(), label: department_name }));

   const designationItems = designations?.map(({ id, designation }) => ({ value: id.toString(), label: designation }));

   const contactNumbers = values.contact_numbers.map((item, index) => (
      <FormListItem
         key={item.key}
         placeholder={`Contact number ${index + 1}`}
         inputProps={getListInputProps('contact_numbers', index, 'contactNumber')}
         onRemove={() => removeListItem('contact_numbers', index)}
      />
   ));

   const addresses = values.addresses.map((item, index) => (
      <FormListItem
         key={item.key}
         placeholder={`Address ${index + 1}`}
         inputProps={getListInputProps('addresses', index, 'address')}
         onRemove={() => removeListItem('addresses', index)}
      />
   ));

   return (
      <>
         <form onSubmit={onSubmit(handleSubmit)}>
            <SimpleGrid cols={2}>
               <TextInput label='First Name' {...getInputProps('first_name')} />
               <TextInput label='Last Name' {...getInputProps('last_name')} />
               <DatePicker label='Birthday' placeholder='Pick date' {...getInputProps('birthday')} icon={<Calendar size={16} />} />
               <TextInput label='NIC' {...getInputProps('nic')} />
               <SelectWithAdd
                  label='Department'
                  value={values.department_id}
                  onChange={handleDepartmentChange}
                  error={errors.department_id}
                  data={departmentItems || []}
                  onAddClick={() => setOpenNewDepartment(true)}
               />
               <SelectWithAdd
                  label='Designation'
                  {...getInputProps('designation_id')}
                  data={designationItems || []}
                  disabled={!values.department_id || isLoading || isFetching}
                  onAddClick={() => setOpenNewDesignation(true)}
               />
               <FormList label='Contact Numbers' items={contactNumbers} onAdd={() => addListItem('contact_numbers', { contactNumber: '', key: randomId() })} />
               <FormList label='Addresses' items={addresses} onAdd={() => addListItem('addresses', { address: '', key: randomId() })} />

            </SimpleGrid>

            <Divider my='sm' />

            <FormActions loading={submitting} reset={reset} />
         </form>
         <CreateDepartmentModal opened={openNewDepartment} onClose={() => setOpenNewDepartment(false)} />
         <CreateDesignationModal opened={openNewDesignation} onClose={() => setOpenNewDesignation(false)} department_id={values.department_id} />
      </>
   )
}

export default UpdateEmployeeForm;                                             