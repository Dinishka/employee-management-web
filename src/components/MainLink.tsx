import { Group, Text, ThemeIcon, UnstyledButton } from '@mantine/core';
import { Link, useMatch, useResolvedPath } from 'react-router-dom';

interface Props {
   icon: React.ReactNode;
   color: string;
   label: string;
   url: string;
}

const MainLink = ({ icon, color, label, url }: Props) => {

   const resolved = useResolvedPath(url);
   const match = useMatch({ path: resolved.pathname, end: true });

   return (
      <div>
         <UnstyledButton
            component={Link}
            to={url}
            sx={(theme) => ({
               display: 'block',
               width: '100%',
               padding: theme.spacing.xs,
               borderRadius: theme.radius.sm,
               color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,
               backgroundColor:
                  (match && theme.colorScheme === 'dark') ? theme.colors.dark[6] :
                  (match && theme.colorScheme === 'light') ? theme.colors.gray[0] :
                  'none'
            })}
         >
            <Group>
               <ThemeIcon color={color} variant='light'>
                  {icon}
               </ThemeIcon>
               <Text size='sm'>{label}</Text>
            </Group>
         </UnstyledButton>
      </div>
   )
}

export default MainLink;