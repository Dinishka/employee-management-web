import { Divider, SimpleGrid, TextInput } from '@mantine/core';
import { DatePicker } from '@mantine/dates';
import { formList, useForm, zodResolver } from '@mantine/form';
import { randomId } from '@mantine/hooks';
import { showNotification } from '@mantine/notifications';
import dayjs from 'dayjs';
import { useState } from 'react';
import { Calendar, Check, X } from 'tabler-icons-react';
import { z } from 'zod';
import { useGetDepartmentsQuery, useLazyGetDepartmentDesignationsQuery } from '../services/departmentService';
import { useCreateEmployeeMutation } from '../services/employeeService';
import CreateDepartmentModal from './CreateDepartmentModal';
import CreateDesignationModal from './CreateDesignationModal';
import FormActions from './FormActions';
import FormList from './FormList';
import FormListItem from './FormListItem';
import SelectWithAdd from './SelectWithAdd';

// Validation schema
const schema = z.object({
   first_name: z.string().trim().min(1, { message: 'First name is required!' }).max(45, { message: 'First name is too long!' }),
   last_name: z.string().trim().min(1, { message: 'Last name is required!' }).max(45, { message: 'Last name is too long!' }),
   birthday: z.date(),
   contact_numbers: z.array(z.object({
      contactNumber: z.string().trim().min(1, { message: 'Please specify a contact number' }).max(10, { message: 'Invalid contact number!' }),
      key: z.string().trim()
   })).nonempty('At least 1 contact number is required!'),
   addresses: z.array(z.object({
      address: z.string().trim().min(1, { message: 'Please specify a address' }),
      key: z.string().trim()
   })).nonempty('At least 1 address is required!'),
   nic: z.string().trim().min(10, { message: 'Invalid NIC!' }).max(12, { message: 'Invalid NIC!' }),
   department_id: z.string().trim().min(1, { message: 'Department is required!' }),
   designation_id: z.string().trim().min(1, { message: 'Designation is required!' })
});

const AddEmployeeForm = () => {
   // Fetch the departments
   const { data: departments } = useGetDepartmentsQuery();
   // Fetch designations related to a given department
   const [fetchDesignations, { isLoading, isFetching, data: designations }] = useLazyGetDepartmentDesignationsQuery();

   // Department creation modal state
   const [openNewDepartment, setOpenNewDepartment] = useState(false);
    // Designation creation modal state
   const [openNewDesignation, setOpenNewDesignation] = useState(false);

   const { onSubmit, reset, getInputProps, values, errors, addListItem, getListInputProps, removeListItem, setFieldValue } = useForm({
      schema: zodResolver(schema),
      initialValues: {
         first_name: '',
         last_name: '',
         birthday: null,
         contact_numbers: formList([{ contactNumber: '', key: randomId() }]),
         addresses: formList([{ address: '', key: randomId() }]),
         nic: '',
         department_id: '',
         designation_id: ''
      }
   });

   // Mutation for creating an employee
   const [createEmployee, { isLoading: submitting }] = useCreateEmployeeMutation();

   const handleSubmit = async (data: typeof values) => {
      // Create the object to match the request body
      const newEmployee = {
         ...data,
         birthday: dayjs(data.birthday).format('YYYY-MM-DD'),
         contact_numbers: data.contact_numbers.map(({ contactNumber }) => contactNumber),
         addresses: data.addresses.map(({ address }) => address),
      }
      // @ts-ignore
      delete newEmployee.department_id;
      try {
         const payload = await createEmployee(newEmployee).unwrap();
         // If success show success message
         showNotification({
            title: 'Success',
            message: payload.message,
            color: 'teal',
            icon: <Check size={18} />
         });
         reset();
      } catch (e: any) {
         // If error show error message
         showNotification({
            title: 'Error',
            message: e.data.message,
            color: 'red',
            icon: <X size={18} />
         });
      }
   }

   // Handler function for handle department change functionality
   const handleDepartmentChange = (newDepartment: string) => {
      setFieldValue('department_id', newDepartment);
      setFieldValue('designation_id', '');
      fetchDesignations(newDepartment);
   }

   // Craete data to render in the department `Select` element 
   const departmentItems = departments?.map(({ id, department_name }) => ({ value: id.toString(), label: department_name }));

   // Craete data to render in the designation `Select` element 
   const designationItems = designations?.map(({ id, designation }) => ({ value: id.toString(), label: designation }));

   // Craete list to render contact numbers
   const contactNumbers = values.contact_numbers.map((item, index) => (
      <FormListItem
         key={item.key}
         placeholder={`Contact number ${index + 1}`}
         inputProps={getListInputProps('contact_numbers', index, 'contactNumber')}
         onRemove={() => removeListItem('contact_numbers', index)}
      />
   ));

   // Craete list to render addresses
   const addresses = values.addresses.map((item, index) => (
      <FormListItem
         key={item.key}
         placeholder={`Address ${index + 1}`}
         inputProps={getListInputProps('addresses', index, 'address')}
         onRemove={() => removeListItem('addresses', index)}
      />
   ));

   return (
      <>
         <form onSubmit={onSubmit(handleSubmit)}>
            <SimpleGrid cols={2}>
               <TextInput label='First Name' {...getInputProps('first_name')} />
               <TextInput label='Last Name' {...getInputProps('last_name')} />
               <DatePicker label='Birthday' placeholder='Pick date' {...getInputProps('birthday')} icon={<Calendar size={16} />} />
               <TextInput label='NIC' {...getInputProps('nic')} />
               <SelectWithAdd
                  label='Department'
                  value={values.department_id}
                  onChange={handleDepartmentChange}
                  error={errors.department_id}
                  data={departmentItems || []}
                  onAddClick={() => setOpenNewDepartment(true)}
               />
               <SelectWithAdd
                  label='Designation'
                  {...getInputProps('designation_id')}
                  data={designationItems || []}
                  disabled={!values.department_id || isLoading || isFetching}
                  onAddClick={() => setOpenNewDesignation(true)}
               />
               <FormList label='Contact Numbers' items={contactNumbers} onAdd={() => addListItem('contact_numbers', { contactNumber: '', key: randomId() })} />
               <FormList label='Addresses' items={addresses} onAdd={() => addListItem('addresses', { address: '', key: randomId() })} />

            </SimpleGrid>

            <Divider my='sm' />

            <FormActions loading={submitting} reset={reset} />
         </form>

         {/* Department creation modal */}
         <CreateDepartmentModal opened={openNewDepartment} onClose={() => setOpenNewDepartment(false)} />
         {/* Designation creation modal */}
         <CreateDesignationModal opened={openNewDesignation} onClose={() => setOpenNewDesignation(false)} department_id={values.department_id} />
      </>
   )
}

export default AddEmployeeForm;                                             