import { Modal, TextInput } from '@mantine/core';
import { useForm, zodResolver } from '@mantine/form';
import { showNotification } from '@mantine/notifications';
import { Check, X } from 'tabler-icons-react';
import { z } from 'zod';
import { useCreateDepartmentMutation } from '../services/departmentService';
import FormActions from './FormActions';

interface Props {
   opened: boolean;
   onClose: () => void;
}

const schema = z.object({
   department_name: z.string().trim().min(1, { message: 'Department name is required!' }).max(45, { message: 'Department name is too long!' })
});

const CreateDepartmentModal = ({ opened, onClose }: Props) => {
   const { onSubmit, getInputProps, reset, values } = useForm({
      schema: zodResolver(schema),
      initialValues: {
         department_name: ''
      }
   });

   const [createDepartment, { isLoading }] = useCreateDepartmentMutation();

   const handleSubmit = async (data: typeof values) => {
      try {
         const payload = await createDepartment(data).unwrap();
         showNotification({
            title: 'Success',
            message: payload.message,
            color: 'teal',
            icon: <Check size={18} />
         });
         reset();
         onClose();
      } catch (e: any) {
         showNotification({
            title: 'Error',
            message: e.data.message,
            color: 'red',
            icon: <X size={18} />
         });
      }
   }

   return (
      <Modal title='Create Department' opened={opened} onClose={onClose}>
         <form onSubmit={onSubmit(handleSubmit)}>
            <TextInput label='Department' placeholder='Enter department name' required {...getInputProps('department_name')} />
            <FormActions loading={isLoading} reset={reset} />
         </form>
      </Modal>
   )
}

export default CreateDepartmentModal;