import { Modal, TextInput } from '@mantine/core';
import { useForm, zodResolver } from '@mantine/form';
import { showNotification } from '@mantine/notifications';
import { Check, X } from 'tabler-icons-react';
import { z } from 'zod';
import { useCreateDesignationMutation } from '../services/designationService';
import FormActions from './FormActions';

interface Props {
   opened: boolean;
   onClose: () => void;
   department_id: string;
}

const schema = z.object({
   designation: z.string().trim().min(1, { message: 'Designation is required!' }).max(45, { message: 'Designation is too long!' })
});

const CreateDesignationModal = ({ opened, onClose, department_id }: Props) => {
   const { onSubmit, getInputProps, reset, values } = useForm({
      schema: zodResolver(schema),
      initialValues: {
         designation: ''
      }
   });

   const [createDesignation, { isLoading }] = useCreateDesignationMutation();

   const handleSubmit = async (data: typeof values) => {
      try {
         const payload = await createDesignation({ ...data, department_id }).unwrap();
         showNotification({
            title: 'Success',
            message: payload.message,
            color: 'teal',
            icon: <Check size={18} />
         });
         reset();
         onClose();
      } catch (e: any) {
         showNotification({
            title: 'Error',
            message: e.data.message,
            color: 'red',
            icon: <X size={18} />
         });
      }
   }

   return (
      <Modal title='Create Designation' opened={opened} onClose={onClose}>
         <form onSubmit={onSubmit(handleSubmit)}>
            <TextInput label='Designation' placeholder='Enter designation' required {...getInputProps('designation')} />
            <FormActions loading={isLoading} reset={reset} />
         </form>
      </Modal>
   )
}

export default CreateDesignationModal;