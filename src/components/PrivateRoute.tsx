import { Navigate, useLocation } from 'react-router-dom';
import { useAppSelector } from '../app/hooks';

interface Props {
   children: JSX.Element
}

const PrivateRoute = ({ children }: Props) => {
   const user = useAppSelector(state => state.user);
   const location = useLocation();

   // Check if user is not authenticated
   if (!user.isSignedIn && !sessionStorage.getItem('access_token')) {
      // Navigate to login page
      return <Navigate to='/login' state={{ from: location }} replace />
   }

   return children;
}

export default PrivateRoute;