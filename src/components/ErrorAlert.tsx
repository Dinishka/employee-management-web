import { Alert } from '@mantine/core';
import type { SerializedError } from '@reduxjs/toolkit';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query/react';
import { AlertCircle } from 'tabler-icons-react';

interface Props {
   error: FetchBaseQueryError | SerializedError;
}

const ErrorAlert = ({ error }: Props) => {
   // @ts-ignore
   const message = 'data' in error ? error.data.message : 'Something went wrong.';

   return (
      <Alert
         icon={<AlertCircle size={16} />}
         title='Error!'
         color='red'
      >
         {message}
      </Alert>
   )
}

export default ErrorAlert;