import type { SelectProps } from '@mantine/core';
import { Box, Button, Select } from '@mantine/core';

interface Props extends SelectProps {
   onAddClick: React.MouseEventHandler<HTMLButtonElement>
}

const SelectWithAdd = ({ onAddClick, disabled, ...other }: Props) => {
   return (
      <Box>
         <Select {...other} disabled={disabled} mb='xs' />
         <Button variant='subtle' compact onClick={onAddClick} disabled={disabled}>+</Button>
      </Box>
   )
}

export default SelectWithAdd;