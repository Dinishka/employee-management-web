export interface Employee {
   id: number;
   first_name: string;
   last_name: string;
   birthday: string;
   addresses: string[];
   contact_numbers: string[];
   nic: string;
   designation_id: number;
   designation: string;
   department_id: number;
   department_name: string;
}

export interface Department {
   id: number;
   department_name: string;
}

export interface Designation {
   id: number;
   designation: string;
}