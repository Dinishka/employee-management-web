import ErrorAlert from '../components/ErrorAlert';
import Loading from '../components/Loading';
import PageLayoutView from '../layout/PageLayoutView';
import { useGetDepartmentsQuery } from '../services/departmentService';

const Departments = () => {
  const { isLoading, isError, error, data } = useGetDepartmentsQuery();

  if (isLoading) {
    return <Loading />
  }

  if (isError) {
    return <ErrorAlert error={error} />
  }

  const tableHeaders = (
    <tr>
      <th>No.</th>
      <th>Department Id</th>
      <th>Department Name</th>
    </tr>
  );

  const rows = data?.map((department, index) => (
    <tr key={department.id}>
      <td>{index + 1}</td>
      <td>{department.id}</td>
      <td>{department.department_name}</td>
    </tr>
  ));

  return <PageLayoutView title='Departments' headers={tableHeaders} rows={rows!} />
}

export default Departments;