import AddEmployeeForm from '../components/AddEmployeeForm';
import PageLayout from '../layout/PageLayout';

const AddEmployee = () => {
  return (
    <PageLayout title='Create Employee'>
      <AddEmployeeForm />
    </PageLayout>
  )
}

export default AddEmployee;