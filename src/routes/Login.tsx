import { Card, Center, createStyles, PasswordInput, TextInput, Title } from '@mantine/core';
import { useForm } from '@mantine/form';
import { showNotification } from '@mantine/notifications';
import { useLocation, useNavigate } from 'react-router-dom';
import { Lock, User, X } from 'tabler-icons-react';
import { useAppDispatch } from '../app/hooks';
import { login } from '../app/userSlice';
import FormActions from '../components/FormActions';
import { useLoginMutation } from '../services/authService';

const useStyles = createStyles((theme) => ({
   loginPage: {
      minHeight: '100vh'
   },
   loginCard: {
      width: '80%',
      maxWidth: '420px'
   },
   title: {
      textAlign: 'center'
   },
   loginForm: {
      marginTop: theme.spacing.sm
   }
}));

const Login = () => {
   const { classes } = useStyles();

   const dispatch = useAppDispatch();

   const navigate = useNavigate();
   const location = useLocation();

   // @ts-ignore
   const from = location.state?.from?.pathname || '/';

   const { onSubmit, reset, getInputProps } = useForm({
      initialValues: {
         username: '',
         password: ''
      }
   });

   const [attemptLogin, { isLoading }] = useLoginMutation();

   const handleSubmit = async (data: any) => {
      try {
         const payload = await attemptLogin(data).unwrap();
         dispatch(login(payload.accessToken));
         
         // If success navigate to the route user originally requested
         navigate(from);
      } catch (e: any) {
         showNotification({
            title: 'Error',
            message: e.data.message,
            color: 'red',
            icon: <X size={18} />
         });
      }
   }

   return (
      <Center className={classes.loginPage}>
         <Card shadow='sm' p='lg' radius='lg' className={classes.loginCard}>
            <Title className={classes.title}>Login</Title>

            <form onSubmit={onSubmit(handleSubmit)} className={classes.loginForm}>
               <TextInput label='Username' placeholder='Enter username' required icon={<User size={16} />} {...getInputProps('username')} />
               <PasswordInput label='Password' placeholder='Enter password' required mt='sm' icon={<Lock size={16} />} {...getInputProps('password')} />
               <FormActions loading={isLoading} reset={reset} />
            </form>
         </Card>
      </Center>
   )
}

export default Login;