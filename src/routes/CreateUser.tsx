import CreateUserForm from '../components/CreateUserForm';
import PageLayout from '../layout/PageLayout';

const CreateUser = () => {
   return (
      <PageLayout title='Create User'>
         <CreateUserForm />
      </PageLayout>
   )
}

export default CreateUser;