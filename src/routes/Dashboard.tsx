import { Center, Title } from '@mantine/core';
import construction from '../assets/dashboard_construction.svg';

const Dashboard = () => {
  return (
    <>
      <Center mt='md'>
        <img src={construction} alt='Dashboard under construction' width={500} />
      </Center>
      <Title mt='lg' sx={{ textAlign: 'center' }}>Dashboard is under construction</Title>
    </>
  )
}

export default Dashboard;