import { ActionIcon, Group } from '@mantine/core';
import { showNotification } from '@mantine/notifications';
import { useNavigate } from 'react-router-dom';
import { Check, Eye, Trash, X } from 'tabler-icons-react';
import ErrorAlert from '../components/ErrorAlert';
import Loading from '../components/Loading';
import PageLayoutView from '../layout/PageLayoutView';
import { useDeleteEmployeeMutation, useGetEmployeesQuery } from '../services/employeeService';

const Employees = () => {
  const { isLoading, isError, error, data: employees } = useGetEmployeesQuery();
  const [deleteEmployee] = useDeleteEmployeeMutation();

  const navigate = useNavigate();

  if (isLoading) {
    return <Loading />
  }

  if (isError) {
    return <ErrorAlert error={error} />
  }

  const handleDeleteEmployee = async (id: number) => {
    try {
      const payload = await deleteEmployee(id).unwrap();
      showNotification({
        title: 'Success',
        message: payload.message,
        color: 'teal',
        icon: <Check size={18} />
      });
    } catch (e: any) {
      showNotification({
        title: 'Error',
        message: e.data.message,
        color: 'red',
        icon: <X size={18} />
      });
    }
  }

  const tableHeaders = (
    <tr>
      <th>No.</th>
      <th>Employee Id</th>
      <th>Name</th>
      <th>NIC</th>
      <th>Designation</th>
      <th>Department</th>
      <th />
    </tr>
  );

  const rows = employees?.map(({ id, first_name, last_name, nic, designation, department_name }, index) => (
    <tr key={id}>
      <td>{index + 1}</td>
      <td>{id}</td>
      <td>{`${first_name} ${last_name}`}</td>
      <td>{nic}</td>
      <td>{designation}</td>
      <td>{department_name}</td>
      <td>
        <Group>
          <ActionIcon
            color='cyan'
            variant='hover'
            onClick={() => navigate(`/employee/${id}`)}
          >
            <Eye size={16} />
          </ActionIcon>
          <ActionIcon
            color='red'
            variant='hover'
            onClick={() => handleDeleteEmployee(id)}
          >
            <Trash size={16} />
          </ActionIcon>
        </Group>
      </td>
    </tr>
  ));

  return <PageLayoutView title='Employees' headers={tableHeaders} rows={rows!} />
}

export default Employees;