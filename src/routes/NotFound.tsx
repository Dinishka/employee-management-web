import { Button, Center, Title } from '@mantine/core';
import { Link } from 'react-router-dom';
import { ArrowLeft } from 'tabler-icons-react';

const NotFound = () => {
  return (
    <Center sx={{ minHeight: '100vh' }}>
      <Title>404</Title>
      <Button component={Link} to='/' leftIcon={<ArrowLeft />} ml='xl'>Go back to Home</Button>
    </Center>
  )
}

export default NotFound;