import { useParams } from 'react-router-dom';
import ErrorAlert from '../components/ErrorAlert';
import Loading from '../components/Loading';
import UpdateEmployeeForm from '../components/UpdateEmployeeForm';
import PageLayout from '../layout/PageLayout';
import { useGetEmployeeByIdQuery } from '../services/employeeService';

const ViewEmployee = () => {
   const params = useParams();
   const { isLoading, isError, error, data } = useGetEmployeeByIdQuery(params.id!);

   if (isLoading) {
      return <Loading />
   }

   if (isError) {
      return <ErrorAlert error={error} />
   }

   return (
      <PageLayout title={`Employee: ${params.id}`}>
         <UpdateEmployeeForm {...data!} />
      </PageLayout>
   )
}

export default ViewEmployee;