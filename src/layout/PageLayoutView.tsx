import { Table } from '@mantine/core';
import PageLayout from './PageLayout';

interface Props {
   title: string;
   headers: JSX.Element;
   rows: JSX.Element[];
}

const PageLayoutView = ({ title, headers, rows }: Props) => {
   return (
      <PageLayout title={title}>
         <Table striped highlightOnHover>
            <thead>{headers}</thead>
            <tbody>{rows}</tbody>
         </Table>
      </PageLayout>
   )
}

export default PageLayoutView;