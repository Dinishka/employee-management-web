import { Navbar } from '@mantine/core';
import { BuildingCommunity, Dashboard, Man, UserPlus, Users } from 'tabler-icons-react';
import MainLink from '../components/MainLink';

interface Props {
   hidden: boolean;
}

const SideBar = ({ hidden }: Props) => {

   return (
      <Navbar width={{ sm: 300 }} p='md' hiddenBreakpoint='sm' hidden={hidden}>
         <Navbar.Section grow>
            <MainLink icon={<Dashboard size={16} />} color='blue' label='Dashboard' url='/' />
            <MainLink icon={<Users size={16} />} color='teal' label='Employees' url='/employees' />
            <MainLink icon={<Man size={16} />} color='violet' label='Create Employee' url='/create_employee' />
            <MainLink icon={<BuildingCommunity size={16} />} color='lime' label='Departments' url='/departments' />
            <MainLink icon={<UserPlus size={16} />} color='yellow' label='Create User' url='/create_user' />
         </Navbar.Section>
      </Navbar>
   )
}

export default SideBar;