import { AppShell, useMantineTheme } from '@mantine/core';
import { useState } from 'react';
import { Outlet } from 'react-router-dom';
import AppHeader from './AppHeader';
import SideBar from './SideBar';

const AppLayout = () => {

   const theme = useMantineTheme();
   const [opened, setOpened] = useState(false);

   return (
      <AppShell
         styles={{
            main: {
               background: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0],
            }
         }}
         navbarOffsetBreakpoint='sm'
         fixed
         header={<AppHeader opened={opened} setOpened={setOpened} theme={theme} />}
         navbar={<SideBar hidden={!opened} />}>

         <Outlet />

      </AppShell>
   )
}

export default AppLayout;