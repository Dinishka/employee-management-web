import { ActionIcon, Burger, Button, createStyles, Header, MantineTheme, MediaQuery, Text, useMantineColorScheme } from '@mantine/core';
import { Logout, MoonStars, Sun } from 'tabler-icons-react';
import { useAppDispatch } from '../app/hooks';
import { logout } from '../app/userSlice';

interface Props {
   opened: boolean;
   setOpened: React.Dispatch<React.SetStateAction<boolean>>;
   theme: MantineTheme;
}

const useStyles = createStyles(() => ({
   header: {
      display: 'flex',
      alignItems: 'center',
      height: '100%'
   }
}));

const AppHeader = ({ opened, setOpened, theme }: Props) => {
   const { classes } = useStyles();

   const { colorScheme, toggleColorScheme } = useMantineColorScheme();
   const dark = colorScheme === 'dark';

   const dispatch = useAppDispatch();

   return (
      <Header height={60} p='md'>
         <div className={classes.header}>
            <MediaQuery largerThan='sm' styles={{ display: 'none' }}>
               <Burger
                  opened={opened}
                  onClick={() => setOpened((o) => !o)}
                  size='sm'
                  color={theme.colors.gray[6]}
                  mr='xl'
               />
            </MediaQuery>

            <Text>Employee Management System</Text>

            <ActionIcon
               variant='outline'
               color={dark ? 'yellow' : 'blue'}
               onClick={() => toggleColorScheme()}
               title='Toggle theme'
               ml='auto'
            >
               {dark ? <Sun size={18} /> : <MoonStars size={18} />}
            </ActionIcon>

            <Button color='red' variant='light' ml='auto' leftIcon={<Logout />} onClick={() => dispatch(logout())}>Log out</Button>
         </div>
      </Header>
   )
}

export default AppHeader;