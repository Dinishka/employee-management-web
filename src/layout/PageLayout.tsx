import { Container, Title } from '@mantine/core';

interface Props {
   title: string;
   children: JSX.Element;
}

const PageLayout = ({ title, children }: Props) => {
   return (
      <Container fluid>
         <Title mb='lg'>{title}</Title>

         {children}
      </Container>
   )
}

export default PageLayout;